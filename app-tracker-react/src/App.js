import React from 'react';
import { Provider } from 'react-redux'
import './App.css';
import Signup from "./components/Signup";
import Login from "./components/Login";
import Location from './components/Location';
import Agencies from './components/Agencies';
import Home from './components/Home'
import { Route, BrowserRouter as Router, Redirect } from 'react-router-dom';
import { Navbar, NavbarBrand } from 'reactstrap';
import store from './redux/store.js'

function App() {
  return (
      <Provider store={store}>
          <div className="App">
            <Router>
              <Navbar dark color="primary">
                <div className="container">
                  <NavbarBrand>Advertisement</NavbarBrand>
                </div>
              </Navbar>
              <Route exact path='/' render={() => <Redirect to='/signup' />} />
                  <Route path='/signup' component={Signup} />
                  <Route path='/login' component={Login} />
                  <Route path='/location' component={Location} />
                  <Route path='/home' component={Home} />
                  <Route path='/addAgencies' component={Agencies} />
              </Router>
          </div>
      </Provider>
  );
}

export default App;
