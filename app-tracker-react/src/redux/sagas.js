import { call, fork, put, takeEvery, takeLatest } from 'redux-saga/effects'
import { getAddressSuccess, getAddressFailure } from '../redux/actions.js'

import { REVERSE_GEOCODE_API_KEY } from '../config.js'

function * getAddress({ latLng }) {
    console.log('latLng', latLng)
    const lat = latLng.lat().toString()
    const lng = latLng.lng().toString()
    try {
        const response = yield call(
            window.fetch,
            `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${REVERSE_GEOCODE_API_KEY}`
        )
        if (response.status === 200) {
            const res = yield response.json().then(value => value)
            if (res.results.length > 0) {
                const { results: [{ formatted_address }] } = res
                yield put(getAddressSuccess(formatted_address))
            } else {
                yield put(getAddressFailure('Failed to get Address'))
            }
        } else {
            yield put(getAddressFailure('Failed to get Address'))
        }
    } catch (e) {
        yield put(getAddressFailure('Failed to get Address'))
    }
}
function * getAddressWatcher() {
    yield takeLatest('GET_ADDRESS_PENDING', getAddress)
}

function* mySaga() {
    yield fork(getAddressWatcher)
}

export default mySaga
