export const getAddressPending = latLng => ({
    type: 'GET_ADDRESS_PENDING',
    latLng
})

export const getAddressFailure = message => ({
    type: 'GET_ADDRESS_FAILURE',
    message
})
export const getAddressSuccess = address => ({
    type: 'GET_ADDRESS_SUCCESS',
    address
})

