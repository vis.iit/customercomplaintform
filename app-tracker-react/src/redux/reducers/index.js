import { combineReducers } from "redux";

const initialState = { locations: [] }
const location = (state = initialState, action) => {
    switch(action.type) {
        case 'GET_ADDRESS_PENDING':
            return {...state, loading: true }
        case 'GET_ADDRESS_SUCCESS':
            return {
                ...state,
                locations: [...state.locations, action.address],
                loading: false
            }
        case 'GET_ADDRESS_FAILURE':
            return {...state, loading: false, message: action.message}
        default:
            return initialState
    }
}

export default combineReducers({ location });

