import React,{Component} from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import { GoogleMap, LoadScript, Marker } from '@react-google-maps/api'
import Modal from 'react-modal';
import Geocode from "react-geocode";
import { REVERSE_GEOCODE_API_KEY } from '../config.js'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { getAddressPending } from '../redux/actions'
import { GOOGLE_MAPS_API_KEY } from '../config.js' 

const customStyles = {
    content : {
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      marginRight           : '-50%',
      transform             : 'translate(-50%, -50%)'
    }
  }


class Home extends Component{

    constructor(props) {
        super(props)
        this.initialMarkerLocation = {
            lat: 15.9128998,
            lng: 79.7399875
        }
        this.state = {
            showLocationModal: false,
            email: '',
            agencies: '',
            state : '',
            startDate: new Date(),
            endDate : new Date(),
            presentLocation: this.initialMarkerLocation
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    selectAgencies = (event) => {
        this.setState ={
            agencies : event.target.value
        }
    }

   

    startDateSelect  = (event) =>{

        console.log(event);
        this.setState({
            startDate : event
        })
        
            
    }

    EndDateSelect = (event) => {
        console.log(event);
        this.setState({
            endDate : event
        })
        
    }
     
    handleSubmit(event) {
        console.log('Current State is: ' + JSON.stringify(this.state));
        alert('Current State is: ' + JSON.stringify(this.state));
        event.preventDefault();
    }

    addLocation = e => {
        e.preventDefault()
        this.setState({ showLocationModal: true })
        navigator.geolocation.getCurrentPosition(position =>{
            console.log(position.coords);
            var currentPosition = position.coords;
            var newPosition = {
                lat : currentPosition.latitude,
                lng : currentPosition.longitude
            }
            this.setState({
                presentLocation : newPosition
            })
        })
        
        // TODO: Show map to allow user to add location
    }
    setLocation = ({ latLng }) => this.setState({ presentLocation: latLng })

    closeModal = () => this.setState({ showLocationModal: false })

   // handleEmailIdChange = ({ target: { value: text } }) => this.setState({ emailId: text })

    //handleCompanyChange = ({ target: { value: text } }) => this.setState({ company: text })

    reverseGeoCodePresentLocation = e => {
        e.preventDefault()
        if (this.state.presentLocation === this.initialMarkerLocation) {
            this.setState({ showLocationModal: false })
        } else {
            this.props.reverseGeoCodePresentLocation(this.state.presentLocation)
            this.setState(state => ({
                showLocationModal: false,
                presentLocation: this.initialMarkerLocation
            }))
        }
    }

    renderInput = props => (
        <input {...props} />
    )

    renderLoader = () => <h5>Loading, Please Wait</h5>
    
     

    render(){

        const { location: { loading, locations = [] } = {} } = this.props

        if (loading) return this.renderLoader()

        return(
            <div>
            <div className="wrapper">
                <div className="form-wrapper">
                    <h3>Set Advertisement Location</h3>
                    <form onSubmit={this.handleSubmit} noValidate>
                        <div className="email">
                            <label htmlFor="email">Advertisement Title</label>
                            <input  placeholder="Enter Advertisement Title" type="email" name="email" noValidate onChange={this.handleChange} /> 
                        </div>
                        <div className="agencies">
                            <label htmlFor="agencies">Agencies</label>
                            <select name="agencies"  onChange= {this.selectAgencies}>
                                        <option value= "Mantra">Mantra</option>
                                        <option value ="Intelizig">Intelizign</option>
                                        <option value = "Accanture">Accanture</option>
                                        <option value = "Google">Google</option>
                                        <option value = "Facebook">FaceBook</option>
                            </select> 
                        </div>
                        {/* <div className="state">
                            <label htmlFor="state">State</label>
                            <select name="state"  onChange= {this.selectState}>
                                        {nameList}
                            </select> 
                        </div> */}
                        <div className="startDate">
                            <label htmlFor="startDate">Start Date</label>
                            <DatePicker 
                                    selected={this.state.startDate}
                                    onChange={this.startDateSelect}
                                />
                        </div>
                        <div className="endDate">
                            <label htmlFor="endDate">End Date</label>
                            <DatePicker 
                                    selected={this.state.endDate}
                                    onChange={this.EndDateSelect}
                                />
                        </div>
                        <div className="location">
                        <label htmlFor="location">Location</label>
                        {locations.map((location, index) => this.renderInput({ key: index, value: location, readOnly: true }))}
                        </div>
                        <div className="createAccount">
                            <button onClick={this.addLocation}>Add Location</button>
                        </div>
                        <div className="createAccount">
                            <button type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>

            <Modal ariaHideApp={false} isOpen={this.state.showLocationModal} onRequestClose={this.closeModal} style={customStyles} contentLabel="Example Modal">
                        <div>
                            <LoadScript id="script-loader" googleMapsApiKey={GOOGLE_MAPS_API_KEY}>
                                <GoogleMap id='map' mapContainerStyle={{ height: "700px", width: "900px" }} zoom={13} center={ this.state.presentLocation }>
                                    <Marker position={this.state.presentLocation} draggable onDragEnd={this.setLocation} />
                                </GoogleMap>
                                <button onClick={this.reverseGeoCodePresentLocation}>
                                    Select Location
                                </button>
                            </LoadScript>
                        </div>
                    </Modal>
            </div>            
        )
    }
}
const mapStateToProps = state => ({
    location: state.location
})

const mapDispatchToProps = {
    reverseGeoCodePresentLocation: getAddressPending
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)