import React, { Component } from 'react'
import { connect } from 'react-redux'
import { GoogleMap, LoadScript, Marker } from '@react-google-maps/api'
import Modal from 'react-modal';
import { Control, LocalForm, Errors } from 'react-redux-form';
import { Breadcrumb, Row,Button, Form, FormGroup, Label, Input, Col } from 'reactstrap';
import { Jumbotron, Container } from 'reactstrap';

import { getAddressPending } from '../redux/actions'
import { GOOGLE_MAPS_API_KEY } from '../config.js' 

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
}

class Location extends Component {
    constructor (props) {
        super(props);

        navigator.geolocation.getCurrentPosition(position =>{
            console.log(position.coords)
        })
        
        this.initialMarkerLocation = {
            lat: 13.0214415,
            lng: 77.6433638
        }
        this.state = {
            showLocationModal: false,
            emailId: '',
            company: '',
            presentLocation: this.initialMarkerLocation
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        console.log('Current State is: ' + JSON.stringify(this.state));
        alert('Current State is: ' + JSON.stringify(this.state));
        event.preventDefault();
    }

    addLocation = e => {
        e.preventDefault()
        // if (navigator.geolocation) {
        //     navigator.geolocation.getCurrentPosition(
        //         position => {
        //             console.log(position.coords);
        //         }
        //     )
        // } 
        this.setState({ showLocationModal: true });
        console.log("geo coding Location");
        // TODO: Show map to allow user to add location
    }
    setLocation = ({ latLng }) => this.setState({ presentLocation: latLng })

    closeModal = () => this.setState({ showLocationModal: false })

   // handleEmailIdChange = ({ target: { value: text } }) => this.setState({ emailId: text })

    //handleCompanyChange = ({ target: { value: text } }) => this.setState({ company: text })

    reverseGeoCodePresentLocation = e => {

        e.preventDefault()
       
        if (this.state.presentLocation === this.initialMarkerLocation) {
            this.setState({ showLocationModal: false })
        } else {
            this.props.reverseGeoCodePresentLocation(this.state.presentLocation)
            this.setState(state => ({
                showLocationModal: false,
                presentLocation: this.initialMarkerLocation
            }))
        }
    }

    renderInput = props => (
        <input {...props} className="location"/>
    )

    
    

    renderLoader = () => <h5>Loading, Please Wait</h5>

    render () {
        const { location: { loading, locations = [] } = {} } = this.props

        if (loading) return this.renderLoader()

        return (
            <Jumbotron>
                <div className="row row-content">
                    <div className="col-12">
                        <h3>Set Advertisement Location</h3>
                    </div>
                    <div className="col-12 col-md-9">
                        <LocalForm onSubmit={(values)=> this.handleSubmit(values)}>
                            <Row className="form-group">
                                <Label htmlFor="emailId" md={2}>Email Id</Label>
                                <Col md={10}>
                                    <Control.text model=".emailId" id="emailId" name="email_id" placeholder="abc@gmail.com" className="form-control" />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="company" md={2}>Company Name</Label>
                                <Col md={10}>
                                    <Control.text model=".company" id="company" name="company" placeholder="Company Name" className="form-control" />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                {/* <Label htmlFor="company" md={2}>Company Name</Label> */}
                                <Col md={10}>
                                {locations.map((location, index) => this.renderInput({ key: index, value: location, readOnly: true }))}
                                </Col>
                            </Row>
                            <Row className="form-group">
        
                                <Col md={{size: 2,offset:2}}>
                                    <Button onClick={this.addLocation}  color="primary">
                                        Add Location
                                    </Button>
                                </Col>
                            </Row>
                        </LocalForm>
                    </div>
                </div>
                <div>
                    <Modal ariaHideApp={false} isOpen={this.state.showLocationModal} onRequestClose={this.closeModal} style={customStyles} contentLabel="Example Modal">
                        <div>
                            <LoadScript id="script-loader" googleMapsApiKey={GOOGLE_MAPS_API_KEY}>
                                <GoogleMap id='map' mapContainerStyle={{ height: "400px", width: "800px" }} zoom={2} center={{ lat: 0, lng: -180 }}>
                                    <Marker position={this.state.presentLocation} draggable onDragEnd={this.setLocation} />
                                </GoogleMap>
                                <button onClick={this.reverseGeoCodePresentLocation}>
                                    Select Location
                                </button>
                            </LoadScript>
                        </div>
                    </Modal>
                </div>
            </Jumbotron>
        )
    }
}

const mapStateToProps = state => ({
    location: state.location
})

const mapDispatchToProps = {
    reverseGeoCodePresentLocation: getAddressPending
}

export default connect(mapStateToProps, mapDispatchToProps)(Location)

