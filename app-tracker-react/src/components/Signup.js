import React,{Component} from 'react';
import { Link } from 'react-router-dom';

const emailRegex = RegExp(
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
  );
  
  const formValid = ({ formErrors, ...rest }) => {
    let valid = true;
  
    // validate form errors being empty
    Object.values(formErrors).forEach(val => {
      val.length > 0 && (valid = false);
    });
  
    // validate the form was filled out
    Object.values(rest).forEach(val => {
      val === null && (valid = false);
    });
  
    return valid;
  };


class Signup extends Component{

    constructor(props) {
        super(props);
    
        this.state = {
          firstName: null,
          email: null,
          password: null,
          mobileNumber : null,
          companyName : null,
          formErrors: {
            firstName: "",
            email: "",
            password: "",
            mobileNumber: "",
            companyName: ""
          }
        };
      }
    
      handleSubmit = e => {
        e.preventDefault();
        var query ={
          "email_id" : this.state.email,
          "name" : this.state.firstName,
          "mobileNumber" : this.state.mobileNumber,
          "companyName"  : this.state.companyName,
          "password"  :  this.state.password
        }
        fetch(`http://localhost:1337/signup`, {
				// mode: 'no-cors',
        method: 'POST',
        body: query,
				headers: {
					Accept: 'application/json',
				},
			}, ).then(response => {
				if (response.ok) {
					response.json().then(json => {
						console.log(json);
						
					});
				}
			});
        if (formValid(this.state)) {
          console.log(`
            --SUBMITTING--
            First Name: ${this.state.firstName}
            Email: ${this.state.email}
            Password: ${this.state.password}
            Mobile:${this.state.mobileNumber}
            company:${this.state.companyName}
          `);
        } else {
          console.error("FORM INVALID - DISPLAY ERROR MESSAGE");
        }
      };
    
      handleChange = e => {
        e.preventDefault();
        const { name, value } = e.target;
        let formErrors = { ...this.state.formErrors };
    
        switch (name) {
          case "firstName":
            formErrors.firstName =
              value.length < 3 ? "minimum 3 characaters required" : "";
            break;
          case "email":
            formErrors.email = emailRegex.test(value)
              ? ""
              : "invalid email address";
            break;
          case "password":
            formErrors.password =
              value.length < 6 ? "minimum 6 characaters required" : "";
            break;
          default:
            break;
        }
    
        this.setState({ formErrors, [name]: value }, () => console.log(this.state));
      };

    render(){
        const { formErrors } = this.state;
        return(
            <div className="wrapper">
                <div className="form-wrapper">
                    <h1>Create Account</h1>
                    <form onSubmit={this.handleSubmit} noValidate>
                        <div className="firstName">
                            <label htmlFor="firstName"><b>Name</b></label>
                            <input className={formErrors.firstName.length> 0 ? "error" : null} placeholder="First Name" type="text" name="firstName" noValidate onChange={this.handleChange} /> {formErrors.firstName.length > 0 && (
                            <span className="errorMessage">{formErrors.firstName}</span> )}
                        </div>
                        <div className="email">
                            <label htmlFor="email">Email</label>
                            <input className={formErrors.email.length> 0 ? "error" : null} placeholder="Email" type="email" name="email" noValidate onChange={this.handleChange} /> {formErrors.email.length > 0 && (
                            <span className="errorMessage">{formErrors.email}</span> )}
                        </div>
                        <div className="password">
                            <label htmlFor="password">Password</label>
                            <input className={formErrors.password.length> 0 ? "error" : null} placeholder="Password" type="password" name="password" noValidate onChange={this.handleChange} /> {formErrors.password.length > 0 && (
                            <span className="errorMessage">{formErrors.password}</span> )}
                        </div>
                        <div className="mobileNumber">
                            <label htmlFor="mobileNumber">Mobile Number</label>
                            <input className={formErrors.mobileNumber.length> 0 ? "error" : null} placeholder="Mobile Number" type="text" name="mobileNumber" noValidate onChange={this.handleChange} /> {formErrors.mobileNumber.length > 0 && (
                            <span className="errorMessage">{formErrors.mobileNumber}</span> )}
                        </div>
                        <div className="companyName">
                            <label htmlFor="Comapny Name">Company Name</label>
                            <input className={formErrors.companyName.length> 0 ? "error" : null} placeholder="Company Name" type="text" name="companyName" noValidate onChange={this.handleChange} /> {formErrors.companyName.length > 0 && (
                            <span className="errorMessage">{formErrors.companyName}</span> )}
                        </div>
                        
                        <div className="createAccount">
                            <button type="submit">Create Account</button>
                            <Link to ="/login"><small>Already Have an Account?</small></Link>
                        </div>
                    </form>
                </div>
            </div>            
        )
    }
}
export default Signup;