import React,{Component} from 'react';
import { Link } from 'react-router-dom';

const emailRegex = RegExp(
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
  );
  
  const formValid = ({ formErrors, ...rest }) => {
    let valid = true;
  
    // validate form errors being empty
    Object.values(formErrors).forEach(val => {
      val.length > 0 && (valid = false);
    });
  
    // validate the form was filled out
    Object.values(rest).forEach(val => {
      val === null && (valid = false);
    });
  
    return valid;
  };


class Agencies extends Component{

    constructor(props) {
        super(props);
    
        this.state = {
          contactPerson: null,
          email: null,
          mobileNumber : null,
          companyName : null,
          address : null,
          formErrors: {
            contactPerson: "",
            email: "",
            mobileNumber: "",
            companyName: "",
            address : ""
          }
        };
      }
    
      handleSubmit = e => {
        e.preventDefault();
    
        if (formValid(this.state)) {
          console.log(`
            --SUBMITTING--
            Contact Person: ${this.state.contactPerson}
            Email: ${this.state.email}
            Mobile:${this.state.mobileNumber}
            company:${this.state.companyName}
           `);
        } else {
          console.error("FORM INVALID - DISPLAY ERROR MESSAGE");
        }
      };
    
      handleChange = e => {
        e.preventDefault();
        const { name, value } = e.target;
        let formErrors = { ...this.state.formErrors };
    
        switch (name) {
          case "contactPerson":
            formErrors.firstName =
              value.length < 3 ? "minimum 3 characaters required" : "";
            break;
          case "email":
            formErrors.email = emailRegex.test(value)
              ? ""
              : "invalid email address";
            break;
          case "companyName":
            formErrors.password =
              value.length < 6 ? "minimum 6 characaters required" : "";
            break;
          case "address":
            formErrors.firstName =
              value.length < 3 ? "minimum 3 characaters required" : "";
            break;  
          default:
            break;
        }
    
        this.setState({ formErrors, [name]: value }, () => console.log(this.state));
      };

    render(){
        const { formErrors } = this.state;
        return(
            <div className="wrapper">
                <div className="form-wrapper">
                    <h1>Add Agencies</h1>
                    <form onSubmit={this.handleSubmit} noValidate>
                        <div className="contactPerson">
                            <label htmlFor="contactPerson"><b>Contact Person</b></label>
                            <input className={formErrors.contactPerson.length> 0 ? "error" : null} placeholder="First Name" type="text" name="contactPerson" noValidate onChange={this.handleChange} /> {formErrors.contactPerson.length > 0 && (
                            <span className="errorMessage">{formErrors.contactPerson}</span> )}
                        </div>
                        <div className="email">
                            <label htmlFor="email"><b>Email</b></label>
                            <input className={formErrors.email.length> 0 ? "error" : null} placeholder="Email" type="email" name="email" noValidate onChange={this.handleChange} /> {formErrors.email.length > 0 && (
                            <span className="errorMessage">{formErrors.email}</span> )}
                        </div>
                        <div className="mobileNumber">
                            <label htmlFor="mobileNumber"><b>Mobile Number</b></label>
                            <input className={formErrors.mobileNumber.length> 0 ? "error" : null} placeholder="Mobile Number" type="text" name="mobileNumber" noValidate onChange={this.handleChange} /> {formErrors.mobileNumber.length > 0 && (
                            <span className="errorMessage">{formErrors.mobileNumber}</span> )}
                        </div>
                        <div className="companyName">
                            <label htmlFor="companyName"><b>Company Name</b></label>
                            <input className={formErrors.companyName.length> 0 ? "error" : null} placeholder="Company Name" type="text" name="companyName" noValidate onChange={this.handleChange} /> {formErrors.companyName.length > 0 && (
                            <span className="errorMessage">{formErrors.companyName}</span> )}
                        </div>
                        
                        <div className="address">
                            <label htmlFor="address"><b>Address</b></label>
                            <input className={formErrors.address.length> 0 ? "error" : null} placeholder="Address" type="text" name="address" noValidate onChange={this.handleChange} /> {formErrors.address.length > 0 && (
                            <span className="errorMessage">{formErrors.address}</span> )}
                        </div>
                        <div className="createAccount">
                            <button type="submit">ADD</button>
                            
                        </div>
                    </form>
                </div>
            </div>            
        )
    }
}
export default Agencies;